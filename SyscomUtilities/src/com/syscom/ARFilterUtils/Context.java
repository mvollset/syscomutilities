package com.syscom.ARFilterUtils;

import com.bmc.arsys.api.ARNativeAuthenticationInfo;
import com.bmc.arsys.api.ARServerUser;
import com.syscom.EQEntry.Settings.ClientSettings;;

public class  Context {
	public static ARServerUser getARServerUser(){
		int port=ClientSettings.getArPort();
		return port==-1?new ARServerUser(ClientSettings.getArUser(),ClientSettings.getArPassword(), null, ClientSettings.getArServer()):
			new ARServerUser(ClientSettings.getArUser(), ClientSettings.getArPassword(), null, ClientSettings.getArServer(),port);
	}
	public static ARServerUser getARsArServerUser(ARNativeAuthenticationInfo info){	
			ARServerUser a=new ARServerUser(info,"",ClientSettings.getArServer());
			int port=ClientSettings.getArPort();
			if(port!=-1)
			{
				a.setPort(port);
			}
			return a; 
			
		
	}
	

}
