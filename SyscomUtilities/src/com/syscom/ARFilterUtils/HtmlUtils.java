package com.syscom.ARFilterUtils;
import java.io.ObjectInputStream.GetField;
import java.lang.reflect.Array;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;
public class HtmlUtils {

	public String parseWellFormedHTML(String html){
		Document doc=Jsoup.parse(html);
		return doc.text();
	}
	public String getPlainTextFormatted(String html){
		Document doc=Jsoup.parse(html);
		return getPlainText(doc);
	}
	public TextWithImages getPlainTextFormattedWithImages(String html){
		Document doc=Jsoup.parse(html);
		return getPlainTextWithImages(doc);
	}
	 private TextWithImages getPlainTextWithImages(Element element) {
		 FormattingVisitor formatter = new FormattingVisitor(true);
	        NodeTraversor traversor = new NodeTraversor(formatter);
	        traversor.traverse(element); // walk the DOM, and call .head() and .tail() for each node
	        TextWithImages twi=new TextWithImages();
	        twi.text=formatter.toString();
	        if(formatter.imageCounter>0){
	        	twi.inlineImages=new ArrayList<ImageFile>();
	        	for(int i=0;i<formatter.imageCounter;i++){
	        		twi.inlineImages.add(formatter.attachments[i]);
	        	}
	        	
	        }
	        return twi;
	}
	public String getPlainText(Element element) {
	        FormattingVisitor formatter = new FormattingVisitor();
	        NodeTraversor traversor = new NodeTraversor(formatter);
	        traversor.traverse(element); // walk the DOM, and call .head() and .tail() for each node
	        return formatter.toString();
	    }
	 public String toPlainText(String encodedText){
		 Document doc=Jsoup.parseBodyFragment(encodedText);
		 return doc.text();
	 }
	
	private class FormattingVisitor implements NodeVisitor {
        private static final int maxWidth = 80;
        private int width = 0;
        private StringBuilder accum = new StringBuilder(); // holds the accumulated text
        private ImageFile[] attachments=new ImageFile[3];
        public int imageCounter=0;
        private boolean getImages=false;
        public FormattingVisitor(boolean extractImages){
        	getImages=extractImages;
        }
        public FormattingVisitor(){
        }
        // hit when the node is first seen
        public void head(Node node, int depth) {
            String name = node.nodeName();
            if(getImages&&name.equals("img")&&node.hasAttr("src")){
            	String s=node.attr("src");
            	int c=extractImage(s);
            	if(c>0)
            	   append(String.format("[INLINE IMAGE:%d]",c));
            }
            else 
            if (node instanceof TextNode)
                append(((TextNode) node).text()); // TextNodes carry all user-readable text in the DOM.
            else if (name.equals("li"))
                append("\n * ");
        }

        // hit when all of the node's children (if any) have been visited
        public void tail(Node node, int depth) {
            String name = node.nodeName();
           if (name.equals("br"))
                append("\n");
            else if (StringUtil.in(name, "p", "h1", "h2", "h3", "h4", "h5"))
                append("\n\n");
            else if (name.equals("a"))
                append(String.format(" <%s>", node.absUrl("href")));
        }

        // appends text to the string builder with a simple word wrap method
        private void append(String text) {
            if (text.startsWith("\n"))
                width = 0; // reset counter if starts with a newline. only from formats above, not in natural text
            if (text.equals(" ") &&
                    (accum.length() == 0 || StringUtil.in(accum.substring(accum.length() - 1), " ", "\n")))
                return; // don't accumulate long runs of empty spaces

            if (text.length() + width > maxWidth) { // won't fit, needs to wrap
                String words[] = text.split("\\s+");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i];
                    boolean last = i == words.length - 1;
                    if (!last) // insert a space if not the last word
                        word = word + " ";
                    if (word.length() + width > maxWidth) { // wrap and reset counter
                        accum.append("\n").append(word);
                        width = word.length();
                    } else {
                        accum.append(word);
                        width += word.length();
                    }
                }
            } else { // fits as is, without need to wrap text
                accum.append(text);
                width += text.length();
            }
        }
        private int extractImage(String srcAttribute){
        	if(!srcAttribute.startsWith("data:image/"))
        		return -1;
        	int semicolonindex=srcAttribute.indexOf(';');
        	if(semicolonindex==-1)
        		return -1;
        	String type=srcAttribute.substring("data:image/".length(),semicolonindex);
        	String fname=String.format("inlineimage%d.%s", imageCounter+1,type.compareToIgnoreCase("jpeg")==0?"jpg":type);
        	int dataindex=srcAttribute.indexOf("base64,", semicolonindex)+7;
        	String src=srcAttribute.substring(dataindex);
        	byte[] res= Base64.decodeBase64(src.getBytes());
        	ImageFile imf=new ImageFile();
        	imf.content=res;
        	imf.filename=fname;
        	attachments[imageCounter++]=imf;
        	return imageCounter;
        }
        public String toString() {
            return accum.toString();
        }
    }
}
