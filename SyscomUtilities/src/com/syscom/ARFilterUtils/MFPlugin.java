package com.syscom.ARFilterUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Value;
import com.bmc.arsys.pluginsvr.plugins.ARFilterAPIPlugin;
import com.bmc.arsys.pluginsvr.plugins.ARPluginContext;
import com.syscom.EQEntry.Settings.ClientSettings;
import com.syscom.EQEntry.ars.EQEntry;
import com.syscom.EQEntry.ars.EntryQuery;

import java.util.regex.*;

import org.apache.log4j.Logger;
public class MFPlugin extends ARFilterAPIPlugin {
	
	static Date nextruleCheck;
	static int rulecheckInterval=600;
	static final int SUBJECT=0;
	static final int BODY=1;
	static final int FROM=2;
	static final int RULE_NAME=536870913;
	static final int MATCH=536870917;
	static final int SORT=536870917;
	static final int FIELD=536870920;
	static Map<String,Pattern> subjectPatterns;
	static Map<String,Pattern> bodyPatterns;
	static Map<String,Pattern> fromPatterns;
	private static final Logger logger = Logger.getLogger(MFPlugin.class);
	public static void init(ARPluginContext context){
		context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_INFO, "Starting SYSCOM MailFilter");
		String conf_file = context.getConfigItem("conf_file");
		if(conf_file != null){
			context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_INFO,conf_file);
			System.setProperty("SYSCOMARS.conf", conf_file);
		}
	
		else
			context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_FATAL,"Could not find Configuration");
		String refreshRate = context.getConfigItem("rule_check_interval");
		if(refreshRate!=null)
		{
			try
			{
				int refreshrate=Integer.parseInt(refreshRate);
				MFPlugin.rulecheckInterval=refreshrate;
				context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_INFO,
						String.format("Rules will be checked %d seconds", refreshrate));
			}
			catch (NumberFormatException nfe) {
				context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_WARNING,
						String.format("%s should be a number, using default interval 600 seconds", refreshRate));

			}
		}
		else
			context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_WARNING,
					"rule_check_interval not set, using default interval 600 seconds");
		new ClientSettings();
		MFPlugin.nextruleCheck=new Date(0);
		try {
			
			MFPlugin.readPatterns(Context.getARServerUser());
		} catch (ARException e) {
			context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_FATAL,"Failed to read rules......" + e.getMessage());
		}
	}

	@Override
	public List<Value> filterAPICall(ARPluginContext context,
			List<Value> arguments) throws ARException {
		MFPlugin.readPatterns();
		String subject=arguments.get(0)!=null?arguments.get(0).toString():null;
		String body=arguments.get(1)!=null?arguments.get(1).toString():null;
		String from=arguments.get(2)!=null?arguments.get(2).toString():null;
		MFResult result=null;
		if(subject!=null&&subject.length()>0)
		{
			logger.debug("Checking subject");
			result=CheckSubject(subject);
		}
		if(result!=null&&result.result&&body!=null&&body.length()>0)
		{
			logger.debug("Checking body");
			result=CheckBody(body);
		}
		if(result!=null&&result.result&&from!=null&&from.length()>0)
		{
			logger.debug("Checking from");
			result=CheckFrom(from);
		}
		return MFResultToResultList(result);
		
		
	}
	

	private static MFResult CheckFrom(String s) {
		for(Entry<String,Pattern> e:MFPlugin.fromPatterns.entrySet()){
			Matcher m=e.getValue().matcher(s);
			logger.debug("Checking "  + s + " against " + e.getValue().pattern());
			if(m.find())
				return new MFResult(false, e.getKey());
				
		}
		return new MFResult(true);
	}

	private static MFResult CheckBody(String s) {
		for(Entry<String,Pattern> e:MFPlugin.bodyPatterns.entrySet()){
			Matcher m=e.getValue().matcher(s);
			logger.debug("Checking "  + s + " against " + e.getValue().pattern());
			if(m.find())
				return new MFResult(false, e.getKey());
				
		}
		return new MFResult(true);
	}

	private List<Value> MFResultToResultList(MFResult result)
	{
		if(result==null)
			return null;
		List<Value> r=new ArrayList<Value>(2);
		r.add(new Value(result.result?1:0));
		if(!result.result)
			r.add(new Value(result.ruleName));
		return r;
	}
	private static MFResult CheckSubject(String s)
	{
		for(Entry<String,Pattern> e:MFPlugin.subjectPatterns.entrySet()){
			logger.debug("Checking "  + s + " against " + e.getValue().pattern());
			Matcher m=e.getValue().matcher(s);
			if(m.find())
			{
				logger.debug("We have a hit");
				return new MFResult(false, e.getKey());
			}
				
		}
		return new MFResult(true);
	}
	private static void readPatterns() throws ARException {
		Date d=new Date();
		if(d.after(nextruleCheck))
		{
			readPatterns(Context.getARServerUser());
		}
		
	}
	private synchronized static void setNextRuleCheck()
	{
		Calendar c=Calendar.getInstance();
		c.add(Calendar.SECOND,rulecheckInterval);
		MFPlugin.nextruleCheck=c.getTime();
	}
	private synchronized static void readPatterns(ARServerUser context)throws ARException
	{
		EntryQuery eq=new EntryQuery(context, "SV2:MF:Rules");
		setNextRuleCheck();
		Integer fields[]={RULE_NAME,MATCH,SORT,FIELD};
		Integer sortFields[]={FIELD,SORT};
		eq.setEntryListFieldInfo(fields);
		eq.setSortinfo(sortFields);
		eq.setQualification("'7'=\"Active\"");
		List<EQEntry> result=eq.execute();
		int i=0;
		subjectPatterns=new HashMap<String,Pattern>();
		bodyPatterns=new HashMap<String,Pattern>() ;
		fromPatterns=new HashMap<String,Pattern>();
		logger.debug(result.size() + " Patterns found");
		while(i<result.size())
		{
			EQEntry entry=result.get(i++);
			Pattern p;
			int f=entry.get(FIELD).getIntValue();
			String name=entry.get(RULE_NAME).toString();
			String strpattern=entry.get(MATCH).toString();
			logger.debug(i+ ": Pattern name:" + name);
			logger.debug(i+ ": Pattern pattern:" + strpattern);
			logger.debug(i+ ": Pattern field:" + f);
			try
			{
				p=Pattern.compile(strpattern, Pattern.CASE_INSENSITIVE);
			}
			catch (PatternSyntaxException e) {
				throw new ARException(1,1,"Syntax error rule:" + name +  e.getMessage());
			}
			catch (IllegalArgumentException e) {
				throw new ARException(1,1,e.getMessage());
			}
			
			switch(f)
			{
				case SUBJECT:
					logger.debug("Subject rule put");
					subjectPatterns.put(name, p);
					break;
				case BODY:
					logger.debug("Body rule put");
					bodyPatterns.put(name, p);
					break;
				case FROM:
					logger.debug("from rule put");
					fromPatterns.put(name, p);
					break;
				default:
					break;
			}
		}
		logger.info("Next check " + nextruleCheck.toString());
	}
	
	
}
