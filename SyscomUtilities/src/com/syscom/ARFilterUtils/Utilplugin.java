package com.syscom.ARFilterUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.AttachmentValue;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Value;
import com.bmc.arsys.pluginsvr.ARPluginConstants;
import com.bmc.arsys.pluginsvr.plugins.ARFilterAPIPlugin;
import com.bmc.arsys.pluginsvr.plugins.ARPluginContext;
import com.syscom.EQEntry.Settings.ClientSettings;

import java.util.regex.*;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.digester.RegexMatcher;
import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;


public class Utilplugin extends ARFilterAPIPlugin {
	private static final Logger logger= Logger.getLogger(Utilplugin.class);
	public static int[] supportedDataTypes = new int[] {
		Constants.AR_DATA_TYPE_NULL, 
		Constants.AR_DATA_TYPE_KEYWORD,
		Constants.AR_DATA_TYPE_INTEGER, 
		Constants.AR_DATA_TYPE_REAL,
		Constants.AR_DATA_TYPE_CHAR, 
		Constants.AR_DATA_TYPE_DIARY,
		Constants.AR_DATA_TYPE_ENUM,
		Constants.AR_DATA_TYPE_TIME,
		Constants.AR_DATA_TYPE_BITMASK,
		Constants.AR_DATA_TYPE_DECIMAL, 
		Constants.AR_DATA_TYPE_ATTACH,
		Constants.AR_DATA_TYPE_CURRENCY, 
		Constants.AR_DATA_TYPE_DATE,
		Constants.AR_DATA_TYPE_TIME_OF_DAY,
		Constants.AR_DATA_TYPE_ULONG,
	};
	public static void init(ARPluginContext context){
		/*context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_INFO, "Starting SYSCOM Utilities");*/
		logger.info("Init Syscom.UtilPlugin........");
		String portString = context.getConfigItem("conf_file");
		if(portString == null){
			context.logMessage(ARPluginContext.PLUGIN_LOG_LEVEL_FATAL,"Missing conf file location");
			}
			else
			
				System.setProperty("SYSCOMARS.conf", portString);
		
	}
	
	@Override
	public List<Value> filterAPICall(ARPluginContext context, List<Value> arguments)
			throws ARException {
			String command=arguments.get(0).toString();
			logger.debug("Command is:" + command);
			/* Reg Exp Commands*/
			if(command.compareTo("matches")==0){
				if(arguments.size()!=4)
					throw new ARException(230500, 1, command + " needs 3 parameters!!");	
				return matches(arguments.get(1).toString(),arguments.get(2).toString(),arguments.get(3).toString());
			}
			if(command.compareTo("find")==0){
				if(arguments.size()!=4)
					throw new ARException(230500, 1, command + " needs 3 parameters!!");	
				return find(arguments.get(1).toString(),arguments.get(2).toString(),arguments.get(3).toString());
			}
			if(command.compareTo("replace")==0){
				if(arguments.size()!=5)
					throw new ARException(230500, 1, command + " needs 4 parameters!!");

				
				return replace(getParameter(arguments.get(1), true),
						getParameter(arguments.get(2),true),
						getParameter(arguments.get(3),true),
						getParameter(arguments.get(4),true));
			}
			/*Random commands*/
			if(command.compareTo("rnd")==0){
				if(arguments.size()!=3)
					throw new ARException(230500, 1, command + " needs 2 parameters!!");
				return rnd(arguments.get(1).getIntValue(),arguments.get(2).getIntValue());
			}
			if(command.compareTo("draw")==0){
				if(arguments.size()!=2)
					throw new ARException(230500, 1, command + " takes 1 and only 1 parameter. If you supply two or three you will get this message. If you however call this without any parameters at all you will still get this error.......");
				return draw(arguments.get(1).getDoubleValue());
			
			}
			if(command.compareTo("drawpercent")==0){
				if(arguments.size()!=2)
					throw new ARException(230500, 1, command + " takes 1 and only 1 parameter. If you supply two or three you will get this message. If you however call this without any parameters at all you will still get this error.......");
				double odds=(double)arguments.get(1).getIntValue() /100;
				return draw(odds);
			
			}
			/*File utils*/
			if(command.compareTo("writefile")==0){
				if(arguments.size()<3)
					throw new ARException(230500, 1, command + " needs both content and filename");
				
				return writeFile(arguments.get(1).toString(),arguments.get(2).toString(),
						arguments.size()>3?arguments.get(3).toString():"UTF-16",
						arguments.size()>4?parseBoolean(arguments.get(4).toString()):false);
			}
			if(command.compareTo("readfile")==0){
				if(arguments.size()<2)
					throw new ARException(230500, 1, command + " needs a filename");
				
				return readFile(arguments.get(1).toString(),
						arguments.size()>2?arguments.get(2).toString():"UTF-16");
			}
			if(command.compareTo("html2formattedtext")==0){
				if(arguments.size()<2)
					throw new ARException(230500, 1, command + " needs a filename");
				HtmlUtils html=new HtmlUtils();
				String s=html.getPlainTextFormatted((arguments.get(1).toString()));
			    return Arrays.asList(new Value(s));
			}
			if(command.compareTo("AND")==0){
				int i1,i2;
				i1=arguments.get(1).getIntValue();
				i2=arguments.get(2).getIntValue();
				return Arrays.asList(new Value(i1&i2));
						
							
			}
			if(command.compareTo("OR")==0){
				int i1,i2;
				i1=arguments.get(1).getIntValue();
				i2=arguments.get(2).getIntValue();
				return Arrays.asList(new Value(i1|i2));
						
							
			}
			if(command.compareTo("XOR")==0){
				int i1,i2;
				i1=arguments.get(1).getIntValue();
				i2=arguments.get(2).getIntValue();
				return Arrays.asList(new Value(i1^i2));
						
							
			}
			if(command.compareTo("NOT")==0){
				int i1;
				i1=arguments.get(1).getIntValue();
				return Arrays.asList(new Value(~i1));
						
							
			}
			if(command.compareTo("<<")==0){
				int i1,i2;
				i1=arguments.get(1).getIntValue();
				i2=arguments.get(2).getIntValue();
				return Arrays.asList(new Value(i1<<i2));
						
							
			}
			if(command.compareTo(">>")==0){
				int i1,i2;
				i1=arguments.get(0).getIntValue();
				i2=arguments.get(2).getIntValue();
				return Arrays.asList(new Value(i1>>i2));
						
							
			}
			if(command.compareTo(">>>")==0){
				int i1,i2;
				i1=arguments.get(1).getIntValue();
				i2=arguments.get(2).getIntValue();
				return Arrays.asList(new Value(i1>>>i2));
						
							
			}
			if(command.compareTo("encodeBase64")==0){
				String s=arguments.get(1).getValue().toString();
				byte[] encodedBytes = Base64.encodeBase64(s.getBytes());
				return Arrays.asList(new Value(new String(encodedBytes)));
			}
			if(command.compareTo("decodeBase64")==0){
				String s=arguments.get(1).getValue().toString();
				byte[] encodedBytes = Base64.decodeBase64(s.getBytes());
				return Arrays.asList(new Value(new String(encodedBytes)));
			}
			if(command.compareTo("version")==0){
				try
				{
				InputStream in = getClass().getResourceAsStream("/build_info.properties");
				Properties properties = new Properties();
				properties.load(in);
				
				String ver=String.format("%s.%s.%s",
						properties.getProperty("build.major.number"),
						properties.getProperty("build.revision.number"),
						properties.getProperty("build.minor.number")
						);
				logger.info("Version is:" + ver);
				return Arrays.asList(new Value(ver));
				}
				catch(NullPointerException x)
				{
					logger.info("Failed to getResourceAsStream.");
					return Arrays.asList(new Value("ERRRRRRR"));
				}
				catch (IOException ex)
				{
					logger.info("Failed to read properties file.........");
					return Arrays.asList(new Value("ERRRRRRR"));
				}
			}
			if(command.compareTo("unencodeHTML")==0){
				HtmlUtils html=new HtmlUtils();
				String s=html.toPlainText(arguments.get(1).toString());
			    return Arrays.asList(new Value(s));
			}
			if(command.compareTo("removeDuplicates")==0){
				Value v=arguments.get(1);
				if(v==null){
					return Arrays.asList(new Value());
				}
				String delimiter;
				if(arguments.size()>2){
					delimiter=arguments.get(2).toString();
				}
				else
					delimiter=";";
				String parts[]=v.toString().split(delimiter);
			    HashSet<String> hs=new HashSet<String>(parts.length);
			    List<String> retlist=new ArrayList<String>(parts.length);
			    for(String part:parts){
			    	if(hs.add(part)){
			    		retlist.add(part);
			    	}
			    }
			    StringBuilder sb=new StringBuilder();
			    for(int i=0;i<retlist.size();i++){
			    	String gs=retlist.get(i);
			    	sb.append(gs + delimiter);
			    }
				return Arrays.asList(new Value(sb.toString()));
			}
			if(command.compareTo("imageExtract")==0){
				new ClientSettings();
				Value v=arguments.get(1);
				HtmlUtils utils=new HtmlUtils();
				TextWithImages twi=
						utils.getPlainTextFormattedWithImages(v.toString());
			    
				ARServerUser serveruser=Context.getARsArServerUser(context);
				Entry e=new Entry();
				UUID uuid=UUID.randomUUID();
				
				e.put(179, new Value(uuid.toString()));
				e.put(1000000151, new Value(twi.text));
				if(twi.inlineImages!=null){
					for(int i=0;i<twi.inlineImages.size();i++){
						AttachmentValue attval=new AttachmentValue(twi.inlineImages.get(i).filename,twi.inlineImages.get(i).content);
						e.put(1000000351+i, new Value(attval));
					}
				}
				
				serveruser.createEntry("SYSCOM:ImageExtractor",e);
				return Arrays.asList(new Value(uuid.toString()),new Value(twi.text));
			}
			throw new ARException(230500, 1, command + " is not a known command");		
	}


	private boolean parseBoolean(String string) {
		if(string.compareTo("true")==0)
			return true;
		return false;
	}

	private List<Value> writeFile(String filename, String Content,String encoding,boolean overwrite) {
		List<Value> ret=new ArrayList<Value>(1);
		try
		{
			logger.debug("Overwrite is:" + (overwrite?"TRUE":"FALSE" ));
			if(!overwrite&&fileExists(filename))
			{
				ret.add(new Value(-1));
				ret.add(new Value("File " + filename + " exists"));
			}
			else
			{
				FileOutputStream fos = new FileOutputStream(filename); 
				OutputStreamWriter out = new OutputStreamWriter(fos, encoding);
				out.write(Content);
				out.close();
				fos.close();
				ret.add(new Value(1));
			}
		}
		catch(IOException ex)
		{
			ret.add(new Value(-1));
			ret.add(new Value(ex.getMessage()));
		}
		catch (Exception e) {
			ret.add(new Value(-1));
			ret.add(new Value(e.getMessage()));
		}
		return ret;
		
	}
	private boolean fileExists(String filename){
		File f= new File(filename);
		if(logger.isDebugEnabled()){
			logger.debug(filename);
			logger.debug("does " + (f.exists()?" exist":" not exist"));
			logger.debug("is a " + (f.isFile()?" file":" something else....."));
		}
		
		return f.exists()&&f.isFile();
	}
	private List<Value> readFile(String filename,String encoding) {
		List<Value> ret=new ArrayList<Value>(1);
		
		try{
			FileInputStream fis = new FileInputStream(filename); 
			InputStreamReader in = new InputStreamReader(fis, encoding);
			BufferedReader bf=new BufferedReader(in);
			StringBuilder sb=new StringBuilder();
			String line;
			while((line=bf.readLine())!=null)
			{
				sb.append(line);
			}
			bf.close();
			fis.close();
			ret.add(new Value(1));
			ret.add(new Value(sb.toString()));
		}
		catch(IOException ex)
		{
			ret.add(new Value(-1));
			ret.add(new Value(ex.getMessage()));
		}
		catch (Exception e) {
			ret.add(new Value(-1));
			ret.add(new Value(e.getMessage()));
		}
		return ret;
		
	}
	private List<Value> replace(String regex, String s,String replacement,String flags) {
		List<Value> ret=new ArrayList<Value>(1);
		if(s!=null){
			
			ret.add(new Value(Pattern.compile(regex, parseRegexpFlags(flags)).matcher(s).replaceAll(replacement)));
			
		}
		return ret;
	}

	private List<Value> matches(String regex,String s,String flags){
		List<Value> ret=new ArrayList<Value>(1);
		Pattern p=Pattern.compile(regex,parseRegexpFlags(flags));
		Matcher matcher=p.matcher(s);
		if(matcher.matches())
		{
			ret.add(new Value(1));
			logger.debug(String.format("%s and %s Matches",regex,s));
			logger.debug(String.format("go for captures, there is/are %d of them",matcher.groupCount()));
			for(int i=0;i<7&&i<=matcher.groupCount();i++){
				//Make a copy because of substring behaviour 
				String found=matcher.group(i);
				String result=new String(found);
				logger.debug(String.format("%d : %s ",i,result));
				ret.add(new Value(result));
			}
		}
		else
		{
			logger.debug("No match at all:-(");
			ret.add(new Value(0));
		}
		
		return ret;		
	}
	private List<Value> find(String regex,String s,String flags){
		List<Value> ret=new ArrayList<Value>(1);
		if(s==null)
		{
			ret.add(new Value(0));
			return ret;
		}
		Pattern p=Pattern.compile(regex,parseRegexpFlags(flags));
		Matcher matcher=p.matcher(s);
		if(matcher.find())
		{
			ret.add(new Value(1));
			logger.debug(String.format("%s and %s Matches",regex,s));
			logger.debug(String.format("go for captures, there is/are %d of them",matcher.groupCount()));
			for(int i=0;i<7&&i<=matcher.groupCount();i++){
				//Make a copy because of substring behaviour 
				String found=matcher.group(i);
				String result=new String(found);
				logger.debug(String.format("%d : %s ",i,result));
				ret.add(new Value(result));
			}
		}
		else
		{
			logger.debug("Nothing to find.......");
			ret.add(new Value(0));
		}
		
		return ret;		
	}
	private List<Value> rnd(int low,int high){
		List<Value> ret=new ArrayList<Value>(1);
		double r=Math.random();
		Long retval=Math.round(r*(high-low)) + low;
		logger.debug("rnd returning:" + retval.toString());
		ret.add(new Value(retval.intValue()));
		return ret;
	}
	private List<Value> draw(double odds)
	{
		logger.debug(String.format("Odds are %f for this to be true",odds));
		List<Value> ret=new ArrayList<Value>(1);
		if(Math.random()<=odds){
			ret.add(new Value(1));
			logger.debug("Draw Returning true");
		}
		else{
			ret.add(new Value(0));
			logger.debug("Draw Returning false");
		}
		
		return ret;
	}
	private String getParameter(Value v,boolean nullEqualsEmpty){
		String retval=null;
		if(v!=null){
			retval=v.toString();
		}
		return retval!=null?retval:(nullEqualsEmpty?"":null);
		
	}
	private int parseRegexpFlags(String options){
		int retval=0;
		for(int i=0;i<options.length();i++){
			char c=options.charAt(i);
			retval=retval|getRegexpFlag(c);
		}
		return retval;
	}
	private int getRegexpFlag(char c){
		switch(c){
			case 'q':
				return Pattern.CANON_EQ;
			case 'i':
				return Pattern.CASE_INSENSITIVE;
			case 'c':
				return Pattern.COMMENTS;
			case 'd':
				return Pattern.DOTALL;
			case 'l':
				return Pattern.LITERAL;
			case 'm':
				return Pattern.MULTILINE;
			case 'u':
				return Pattern.UNICODE_CASE;
			case 'x':
				return Pattern.UNIX_LINES;
			default:
				return 0;
					
		}
				
	}

}
